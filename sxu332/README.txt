## OMSCS: Machine Learning - Assignment 4 
## Credit to Juan J. San Emeterio
## Modified by Shu Xu
## Email: shu.xu@gatech.edu

## Software: Java-1.8, IntelliJ, R studio

This readme file is to offer the reader the steps required to make use of this BURLAP machine learning package with several custom classes.
This readme is split by the different Markov Decision Processes that were required for completion of the assignment. The instructions below will assume that you have already successfully downloaded and opened Eclipse.

## Running the Grid World: Low Difficulty Analysis:

1. While inside the directory structure ./src/burlap/assignment4/ right-click on the EasyGridWorldLauncher.
2. Go to the “Run As…” section and select “Java Application.
3. All three algorithms will run and the aggregate analysis and optimal policies will be printed to the console.

## Running the Grid World: High Difficulty Analysis:

1. While inside the directory structure ./src/burlap/assignment4/ right-click on the HardGridWorldLauncher.
2. Go to the “Run As…”n section and select “Java Application.
3. All three algorithms will run and the aggregate analysis and optimal policies will be printed to the console.

## Sample Output

GUI interfaces:
 - Grid World
 - Value Iteration
 - Policy Iteration
 - Q Learning

 The results of different iteration were saved in the csv files under Results fold.

 ## Plot the result of csv files:
Run the plot_results.Rmd to generate the graphs for the report.

## Screen shots of experiments were saved under folder screenshots


